from dataclasses import dataclass
from functools import total_ordering
from heapq import heappush, heappop
from uuid import uuid4
from math import sqrt, pi
from typing import Any, List, Set, Tuple, Optional, Dict, cast


@total_ordering
@dataclass
class Point:
    x: float
    y: float

    __slots__ = ('x', 'y', 'id', '_data')

    def __post_init__(self):
        self.id = uuid4()  # in real world id could be linked with insertion time likely

    @property
    def data(self) -> Any:
        return self._data if hasattr(self, '_data') else None

    @data.setter
    def data(self, data: Any):
        self._data = data

    def dist(self, other: 'Point') -> float:
        # without sqrt for comparison speedup
        return (other.x - self.x) ** 2 + (other.y - self.y) ** 2

    def __lt__(self, other):
        return self.x < other.x and self.y < other.y

    def __eq__(self, other):
        return self.x == other.x and self.y == other.y and self.data == other.data

    def __hash__(self):
        cp = ((self.x + self.y) * (self.x + self.y + 1) / 2) + self.y
        return hash(cp) + hash(self.data) + hash(self.id)


@dataclass
class Rectangle:
    width: float
    height: float
    corner: Point

    __slots__ = ('corner', 'width', 'height')

    @property
    def center(self) -> Point:
        x = self.corner.x + self.width / 2
        y = self.corner.y + self.height / 2
        return Point(x, y)

    def contains_rect(self, other: 'Rectangle') -> bool:
        x, y, w, h = self.corner.x, self.corner.y, self.width, self.height
        ox, oy, ow, oh = other.corner.x, other.corner.y, other.width, other.height

        if ox + ow < x + w and ox > x and oy + oh < y + h and oy > y:
            return True
        else:
            return False

    def __hash__(self):
        cp = ((self.width + self.height) * (self.width + self.height + 1) / 2) + self.height
        return hash(cp) + hash(self.corner)


@dataclass
class Circle:
    center: Point
    radius: float

    __slots__ = ('center', 'radius')

    def check_rect_intersect(self, rect: Rectangle) -> bool:
        cx = self.center.x
        cy = self.center.y
        rx = rect.corner.x
        ry = rect.corner.y
        # rect is not rotated
        dx = cx - max(rx, min(cx, rx + rect.width))
        dy = cy - max(ry, min(cy, ry + rect.height))
        return dx ** 2 + dy ** 2 < self.radius ** 2

    def contains_point(self, point: Point) -> bool:
        return (point.x - self.center.x) ** 2 + (point.y - self.center.y) ** 2 < self.radius ** 2


class Quadrant:
    _max_tree_depth = 20  # depends on data density

    __slots__ = ('bbox', 'max_quad_points', 'points', 'parent', 'children', '_depth')

    def __init__(self,
                 bbox: Rectangle,
                 max_quad_points: int,
                 parent: Optional['Quadrant'] = None,
                 _depth: int = 0) -> None:

        self.bbox = bbox
        self.max_quad_points = max_quad_points
        self.parent = parent
        self._depth = _depth

        self.points: List[Point] = []
        self.children: List['Quadrant'] = []

    def _insert(self,
                x: float,
                y: float,
                data: Optional[Any] = None):

        point = Point(x, y)
        if data:
            point.data = data

        if len(self.children) == 0:
            self.points.append(point)

            if len(self.points) > self.max_quad_points and self._depth < self._max_tree_depth:
                self._split_quadrant()
        else:
            self._children_insert(point)

    def _split_quadrant(self):
        cx = self.bbox.center.x
        cy = self.bbox.center.y
        qw = self.bbox.width / 2
        qh = self.bbox.height / 2
        q_ne_corner = Point(cx, cy)
        q_se_corner = Point(cx, cy - qh)
        q_sw_corner = Point(cx - qw, cy - qh)
        q_nw_corner = Point(cx - qw, cy)

        self.children = [Quadrant(Rectangle(qw, qh, corner), self.max_quad_points, self, self._depth + 1)
                         for corner in (q_ne_corner, q_se_corner, q_sw_corner, q_nw_corner)]

        while self.points:
            self._children_insert(self.points.pop())

    def _children_insert(self, point: Point):
        self._find_quadrant(self, point)._insert(point.x, point.y, point.data)

    @staticmethod
    def _find_quadrant(parent: 'Quadrant', point: Point) -> 'Quadrant':
        px = point.x
        py = point.y
        cx = parent.bbox.center.x
        cy = parent.bbox.center.y
        child_ix = 0  # NE

        if px >= cx:
            if py <= cy:
                child_ix = 1  # SE
        elif px <= cx:
            if py <= cy:
                child_ix = 2  # SW
            elif py >= cy:
                child_ix = 3  # NW

        return parent.children[child_ix]

    def __iter__(self):
        for child in self.children:
            if child.children:
                for subchild in child:
                    yield subchild
            yield child


class QuadTree(Quadrant):
    _points_total: int = 0
    _search_scale_factor: float = sqrt(2)

    def __init__(self,
                 rect: Optional[Rectangle] = None,
                 max_quad_points: int = 4,
                 **kwargs) -> None:

        if not rect:
            bbox = self.get_init_bbox(**kwargs)
        else:
            bbox = rect

        search_scale_factor = kwargs.get('search_scale_factor')
        if search_scale_factor:
            self._search_scale_factor = search_scale_factor

        super().__init__(bbox, max_quad_points)

    @classmethod
    def get_init_bbox(cls, **kwargs):  # used in UniqueQuadTree

        x: Optional[float] = kwargs.get('x')
        y: Optional[float] = kwargs.get('y')
        w: Optional[float] = kwargs.get('width')
        h: Optional[float] = kwargs.get('height')

        if any((x, y, w, h)):
            if not all((x, y, w, h)):
                raise QuadTreeError('Bounding box Rectangle class instance or x, y, width, height are required!')
        else:
            x, y, w, h = 0, 0, 1, 1

        return Rectangle(w, h, Point(x, y))

    @property
    def points_total(self):
        return self._points_total

    def _find_point_parent(self, point: Point) -> Quadrant:
        quad = self._find_quadrant(self, point)
        while not len(quad.children) == 0:
            quad = self._find_quadrant(quad, point)
        return quad

    def find_k_nearest(self,
                       x: float,
                       y: float,
                       k: int,
                       unique_data: bool = False) -> List[Tuple[float, Point]]:

        if self._points_total == 0:
            raise QuadTreeError('Tree is empty. Add some points first.')

        target_point = Point(x, y)
        search_circle, search_rect = self._get_search_primitives(target_point, k)

        pd_heap: List[Tuple[float, Point]] = []
        added_points: Any = set() if not unique_data else {}
        checked_quads: Set[Quadrant] = set()

        while True:
            candidates = self._get_candidate_points(search_circle, search_rect, checked_quads)
            if not candidates and search_circle.radius > sqrt(2 * self.bbox.width ** 2):
                k = len(added_points)
                break  # all points checked, return as is
            for p in candidates:
                if unique_data:
                    dist = target_point.dist(p)
                    if p.data not in added_points or added_points[p.data][0] > dist:
                        added_points[p.data] = (dist, p)
                else:
                    if p not in added_points:
                        added_points.add(p)
                        heappush(pd_heap, (target_point.dist(p), p))

            if len(added_points) >= k:
                break

            search_circle, search_rect = self._get_search_primitives(target_point,
                                                                     k,
                                                                     len(added_points),
                                                                     search_circle.radius)
        if unique_data:
            for ud_point in added_points.values():
                heappush(pd_heap, ud_point)

        return [heappop(pd_heap) for _ in range(k)]

    def _get_search_primitives(self,
                               target_point: Point,
                               k: int,
                               found_points: int = 0,
                               prev_sr: Optional[float] = None) -> Tuple[Circle, Rectangle]:
        # intersection check
        if prev_sr:
            search_remaining_factor = 1 + (k - found_points) / k
            search_radius = prev_sr * self._search_scale_factor * search_remaining_factor
        else:
            search_radius = sqrt(k / (self.points_total * pi)) * self.bbox.width
        search_circle = Circle(target_point, search_radius)
        # containment cheap check (rectangle inscribed in search_circle)
        rect_side = sqrt(2 * search_radius ** 2)
        search_rect = Rectangle(rect_side, rect_side,
                                Point(target_point.x - rect_side / 2,
                                      target_point.y - rect_side / 2))

        # debug. draw search primitives
        if __name__ == '__main__':
            global pass_num
            pass_num += 1
            print(f'Pass - {pass_num}')
            c.create_circle(search_circle.center.x, search_circle.center.y, search_circle.radius,  # type: ignore
                            fill="blue", outline="#DDD", width=3)
            c.create_rectangle(search_rect.corner.x,
                               search_rect.corner.y + search_rect.height,
                               search_rect.corner.x + search_rect.width,
                               search_rect.corner.y, fill="green", outline="#DDD", width=3)

        return search_circle, search_rect

    def _get_candidate_points(self,
                              sc: Circle,
                              sr: Rectangle,
                              checked_quads: Set[Quadrant]) -> List[Point]:

        candidates = []

        def _get_all_points(quad):
            if len(quad.children) != 0:
                for child in quad.children:
                    yield from _get_all_points(child)
            else:
                for p in quad.points:
                    yield p

        def check_tree(quad):
            for child in quad.children:
                if child not in checked_quads:

                    if sr.contains_rect(child.bbox):
                        # quad and all child quads are in search rectangle
                        # so add all points recursively and skip next time
                        checked_quads.add(child)
                        for point in _get_all_points(child):
                            candidates.append(point)

                    elif sc.check_rect_intersect(child.bbox):
                        # search circle intersects quad
                        if len(child.children) == 0:
                            sr_checked_point_num = 0
                            for point in child.points:
                                if sc.contains_point(point):
                                    candidates.append(point)
                                    sr_checked_point_num += 1
                            # no more points in this quad, skip next time
                            if sr_checked_point_num == len(child.points):
                                checked_quads.add(child)
                        else:
                            check_tree(child)

        if len(self.children) == 0 and self not in checked_quads:
            # only one quad tree case
            candidates.extend(self.points)
            checked_quads.add(self)

        check_tree(self)
        return candidates

    def insert(self,
               x: float,
               y: float,
               data: Optional[Any]=None):
        self._points_total += 1
        self._insert(x, y, data)


class QuadTreeError(Exception):
    pass


class UniqueQuadTree:

    def __init__(self,
                 bbox: Optional[Rectangle] = None,
                 max_quad_points: int = 4,
                 search_scale_factor: float = sqrt(2),
                 **kwargs) -> None:

        self.qtree_bbox = bbox if bbox else QuadTree.get_init_bbox(**kwargs)
        self.max_quad_points = max_quad_points
        self.search_scale_factor = search_scale_factor
        self.unique_data_trees: Dict[Any, QuadTree] = {}

    def insert(self,
               x: float,
               y: float,
               data: Any):

        if data in self.unique_data_trees:
            qtree = self.unique_data_trees[data]
        else:
            qtree = self.unique_data_trees[data] = QuadTree(self.qtree_bbox,
                                                            self.max_quad_points,
                                                            search_scale_factor=self.search_scale_factor)
        qtree.insert(x, y)

    def find_k_nearest(self,
                       x: float,
                       y: float,
                       k: int) -> List[Tuple[float, Point]]:

        unique_points: List[Tuple[float, Point]] = []
        for data, qtree in self.unique_data_trees.items():
            # could be better to write optimized for k=1 version of qtree.find_k_nearest
            nearest = qtree.find_k_nearest(x, y, 1)[0]
            nearest[1].data = data
            heappush(unique_points, nearest)

        return [heappop(unique_points) for _ in range(k)]


if __name__ == '__main__':
    import random
    from functools import partial
    from time import time
    from tkinter import Tk, Canvas

    seed = random.randint(0, 1000)
    print('Current seed - ', seed)
    random.seed(seed, version=2)

    SCALE_FACTOR = 1000  # for visualization purposes only (normally data should be normalized 0..1)
    UNIQUE_DATA = True  # only return k-nearest point with unique data(names)
    # UniqueQuadTree сreates separate tree for each unique data entry(name)
    # it's worse then one QuadTree for evenly distributed points, but in case of real data could be better
    USE_UniqueQuadTree = False
    TARGET_X = 0.333 * SCALE_FACTOR  # search start point
    TARGET_Y = 0.444 * SCALE_FACTOR
    TOTAL_POINTS = 10000  # generate N random points
    K = 100  # search for k-nearest (if less found, return as-is by default)
    # more TOTAL_POINTS with same K, more speedup comparing to brute force
    MAX_QUAD_POINTS = 4  # maxumim points in one quad
    UID_ADD_TIMES = (TOTAL_POINTS / K) * 0.5  # each unique data(string) generate times
    SEARCH_SCALE_FACTOR = sqrt(2)  # how fast search area size should increase (depends on data density)

    pass_num = 0
    tk = Tk()

    # setup Tk
    def _create_circle(self, x, y, r, **kwargs):
        return self.create_oval(x - r, y - r, x + r, y + r, **kwargs)
    Canvas.create_circle = _create_circle  # type: ignore

    c = Canvas(tk, width=1000, height=1000, bg='white')
    c.pack()

    # create Tree
    tree = UniqueQuadTree if USE_UniqueQuadTree else QuadTree
    idx = tree(Rectangle(1*SCALE_FACTOR, 1*SCALE_FACTOR, Point(0, 0)),
               max_quad_points=MAX_QUAD_POINTS,
               search_scale_factor=SEARCH_SCALE_FACTOR)

    # feed tree with random generated data
    uid_occ = 1
    uid = uuid4().hex[:6].upper()
    all_points = []
    for point in [(random.random() * SCALE_FACTOR, random.random() * SCALE_FACTOR) for _ in range(TOTAL_POINTS)]:
        if UID_ADD_TIMES <= uid_occ:
            uid = uuid4().hex[:6].upper()
            uid_occ = 1
        else:
            uid_occ += 1

        p = Point(*point)
        p.data = uid
        all_points.append(p)

        idx.insert(*point, data=uid)

    # find_k_nearest using quadtree
    t1 = time()
    part = partial(idx.find_k_nearest, TARGET_X, TARGET_Y, K)
    if USE_UniqueQuadTree:
        sel: List[Tuple[float, Point]] = part()
    else:
        sel = part(UNIQUE_DATA)
    print('QTree time - ', time() - t1)

    # brute force sort data
    distances = []
    added_points: Any = set() if not UNIQUE_DATA else {}
    target_point = Point(TARGET_X, TARGET_Y)
    t1 = time()
    for p in all_points:
        if UNIQUE_DATA:
            dist = target_point.dist(p)
            if p.data not in added_points or added_points[p.data][0] > dist:
                added_points[p.data] = (dist, p)
        else:
            if p not in added_points:
                distances.append((target_point.dist(p), p))
                added_points.add(p)

    if UNIQUE_DATA:
        distances = [val for val in added_points.values()]

    distances.sort()
    brute: List[Tuple[float, Point]] = distances[:K]
    print('Brute force time - ', time() - t1)

    if USE_UniqueQuadTree:
        # can't draw multiple trees
        exit(0)

    # draw all quadrants and points
    pix = 0
    for node in idx:
        c.create_rectangle(node.bbox.corner.x,
                           node.bbox.corner.y + node.bbox.height,
                           node.bbox.corner.x + node.bbox.width,
                           node.bbox.corner.y)

        for p in node.points:
            c.create_oval(p.x, p.y, p.x, p.y, outline="black", fill="black", width=2)
            pix += 1

    for sp in sel:
        p = sp[1]
        c.create_oval(p.x, p.y, p.x, p.y, outline="red", fill="red", width=4)

    c.create_oval(TARGET_X, TARGET_Y, TARGET_X, TARGET_Y, outline="white", fill="red", width=4)

    # Tests
    if UNIQUE_DATA:
        data_found: Any = set()
        for sp in sel:
            p = sp[1]
            if p.data in data_found:
                print('Non-unuqie data found!')
            else:
                data_found.add(p.data)

    ix = 0
    for bt, st in zip(brute, sel):
        ix += 1
        if bt not in sel:
            b, s = bt[1], st[1]
            c.create_oval(b.x, b.y, b.x, b.y, outline="gray", fill="gray", width=4)
            c.create_oval(s.x, s.y, s.x, s.y, outline="pink", fill="pink", width=4)
            print(f'at - {ix} brute - {target_point.dist(b)}, sel - {target_point.dist(s)}')

    sel_dist = [el[0] for el in sel]
    brute_dist = [el[0] for el in brute]
    for bd, sd in zip(brute_dist, sel_dist):
        if bd != sd:
            print(f'Brute force result position mismatch:\n'
                  f'position at tree res - {sel_dist.index(bd)}\n'
                  f'position at brute res - {brute_dist.index(sd)}')

    tk.mainloop()
