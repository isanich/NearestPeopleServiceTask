FROM python:3.7.0-stretch
COPY . /app
WORKDIR /app
RUN pip install pipenv
RUN pipenv install
ENTRYPOINT ["/usr/local/bin/pipenv", "run", "python", "app.py"]
EXPOSE 80