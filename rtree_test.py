import random
from uuid import uuid4
from collections import OrderedDict
from datetime import datetime
from itertools import islice
from time import time
from typing import List, Tuple, Optional, Any, Sequence, Dict, Iterator

from rtree import index


def gen_spots(seed: Optional[Any] = None,
              spots_num: int = 100) -> List[Tuple[float, float, str]]:

    spots: List[Tuple[float, float, str]] = []
    if seed is None:
        seed = datetime.now()
    random.seed(a=seed, version=2)

    uid_occ = 1
    uid = uuid4().hex[:6].upper()
    for _ in range(spots_num):
        if UID_ADD_TIMES <= uid_occ:
            uid = uuid4().hex[:6].upper()
            uid_occ = 1
        else:
            uid_occ += 1
        spots.append((random.random() * SCALE_FACTOR, random.random() * SCALE_FACTOR, uid))

    return spots


def generate_idx_data(spots: List[Tuple[float, float, str]])\
        -> Iterator[Tuple[int, Tuple[float, float, float, float], str]]:
    for ix, spot in enumerate(spots):
        x = spot[0]
        y = spot[1]
        person = spot[2]
        yield (ix, (x, y, x, y), person)


def get_nearest_unique_people(idx: index.Rtree,
                              target_point: Tuple[float, float],
                              target_num_people: int) -> Dict[str, float]:

    start_at = 0
    stop_at = target_num_people
    nearest_people: Dict[str, float] = OrderedDict()

    while True:
        iterator = idx.nearest(coordinates=target_point, num_results=stop_at, objects=True)
        for obj in islice(iterator, start_at, stop_at):
            if obj.object not in nearest_people:
                dist = (obj.bbox[0] - target_point[0]) ** 2 + (obj.bbox[1] - target_point[1]) ** 2
                nearest_people[obj.object] = dist

        if len(nearest_people) < target_num_people:
            start_at = stop_at
            stop_at += stop_at
        else:
            break

    return nearest_people


if __name__ == '__main__':
    SCALE_FACTOR = 100
    TARGET_X = 0.33 * SCALE_FACTOR  # search start point
    TARGET_Y = 0.44 * SCALE_FACTOR
    K = 100
    UID_ADD_TIMES = (2000 / K) * 0.5

    idx = index.Rtree('bulk', generate_idx_data(gen_spots(spots_num=20000)))
    t1 = time()
    p = get_nearest_unique_people(idx, (TARGET_X, TARGET_Y), target_num_people=K)
    print('RTree time - ', time() - t1)
