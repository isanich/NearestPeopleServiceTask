import argparse
import asyncio
import base64
import json
import os
import types
import traceback
import logging
from cryptography.fernet import Fernet
from functools import partial, wraps
from concurrent.futures import ThreadPoolExecutor
from uuid import uuid4
from typing import Dict, Optional, List, Any, Iterable, Coroutine

import aiofiles
from aiohttp import web
from aiohttp_session import setup, get_session
from aiohttp_session.cookie_storage import EncryptedCookieStorage
from jsonschema import Draft4Validator, ValidationError

from qtree import QuadTree
from logs import log


log = log.getChild(__name__)
app_route_table = web.RouteTableDef()


class BaseView(web.View):

    def validate_json(self, json_data: Dict[Any, Any], schem_name: str):
        app = self.request.app
        try:
            Draft4Validator(app['json_schemas'][schem_name]).validate(json_data)
        except ValidationError as e:
            raise web.HTTPBadRequest(text=f'Json validation error! {e}')

    async def get_uid(self):
        session = await get_session(self.request)
        if 'uid' not in session:
            uid = self.generate_uuid_str()
            session['uid'] = uid
        else:
            uid = session['uid']
        return uid

    @staticmethod
    def generate_uuid_str():
        uuid = base64.urlsafe_b64encode(uuid4().bytes)
        return str(uuid, encoding='utf8').replace('=', '')


def validate_json(schema_name):
    def wrapper(f):
        @wraps(f)
        async def inner_wrapper(*args, **kwargs):
            self = args[0]
            json_data = await self.request.json()
            self.validate_json(json_data, schema_name)
            return await f(*args, json_data, **kwargs)
        return inner_wrapper
    return wrapper


@app_route_table.view('/qtree')
class QTree(BaseView):

    def _get_qtree(self, uid: str):
        qtree = self.request.app['idxs'].get(uid, None)
        if not qtree:
            qtree = self.request.app['idxs'][uid] = QuadTree()
        return qtree

    @validate_json('knearest')
    async def post(self, json_data):
        app = self.request.app

        uid = await self.get_uid()
        if uid not in app['idxs']:
            raise web.HTTPBadRequest(text=f'No points added! Add some first.')

        qtree = self._get_qtree(uid)

        x = json_data['point']['x']
        y = json_data['point']['y']
        k = json_data['k']
        unique = json_data['unique_data']
        # run in thread to prevent asyncio blocking on heavy computations
        res = await app.run_in_executor(qtree.find_k_nearest, x, y, k, unique)

        json_res = []
        for el in res:
            p = el[1]
            json_res.append({
                'dist': el[0],
                'data(name)': p.data,
                'point': {
                    'x': p.x,
                    'y': p.y
                },
                'id': str(p.id)  # in real example id could be insertion time based most likely
            })

        return web.json_response(json_res, status=web.HTTPOk.status_code)

    @validate_json('add_point')
    async def put(self, json_data):
        uid = await self.get_uid()
        qtree = self._get_qtree(uid)

        x = json_data['point']['x']
        y = json_data['point']['y']
        data = json_data['name']
        qtree.insert(x, y, data)

        return web.json_response({'message': 'Point is successfully added!'},
                                 status=web.HTTPOk.status_code)


class AppFactory:

    def __init__(self,
                 host: str,
                 port: int,
                 route_tables: Iterable[web.RouteTableDef],
                 debug: bool = False,
                 secret: Optional[bytes] = None,
                 events: Optional[Dict[str, Iterable[Coroutine]]] = None,
                 middlewares: Optional[Iterable[web.middleware]] = None,
                 app_args: Optional[Dict[Any, Any]] = None) -> None:

        self.host = host
        self.port = port

        self.secret = secret
        if not secret:
            if debug:
                self.secret = Fernet.generate_key()
            else:
                raise AppCreationError('Secret key must be provided!')

        self.route_tables = route_tables
        self.events = events if events else {}
        self.middlewares = middlewares if middlewares else []
        self.app_args = app_args if app_args else []

        self.app = self.make_app()
        self.app['debug'] = debug
        self.stop_app = asyncio.Event()

    def make_app(self):
        app = web.Application(middlewares=self.middlewares)
        secret_key = base64.urlsafe_b64decode(self.secret)
        setup(app, EncryptedCookieStorage(secret_key))

        for rtable in self.route_tables:
            app.router.add_routes(rtable)

        # better way could be to make decorator for registering events
        for event_name in self.events.keys():
            event_handlers = self.events[event_name]
            for handler in event_handlers:
                getattr(app, event_name).append(handler)

        for key, arg in self.app_args.items():
            app[key] = arg

        app.thread_exc = ThreadPoolExecutor()
        # bad practice, temporal solution
        app.run_in_executor = types.MethodType(_run_in_executor, app)

        return app

    async def run_app(self):
        runner = web.AppRunner(self.app, handle_signals=True)
        await runner.setup()
        site = web.TCPSite(runner, self.host, self.port)
        try:
            await site.start()
            print(f'Serving at {self.host}:{self.port} ...')
            await self.stop_app.wait()
        finally:
            await runner.cleanup()


class AppCreationError(Exception):
    pass


async def _run_in_executor(app, f, *fargs, wait=30, **fkwargs):
    if asyncio.iscoroutinefunction(f) or asyncio.iscoroutine(f):
        raise TypeError('Coroutines cannot be used with run_in_executor!')

    part = partial(f, *fargs, **fkwargs)
    loop = asyncio.get_event_loop()
    future = loop.run_in_executor(app.thread_exc, part)
    return await asyncio.wait_for(future, wait, loop=loop)


async def load_json_schemas(app: web.Application):
    root = os.path.dirname(os.path.abspath(__file__))
    schemas_dir = os.path.join(root, 'schemas')
    schemas_fnames = [f for f in os.listdir(schemas_dir) if f.endswith('.json')]
    json_schemas = {}

    for fname in schemas_fnames:
        async with aiofiles.open(os.path.join(schemas_dir, fname)) as f:
            json_schemas[fname.replace('.json', '')] = json.loads(await f.read())

    app['json_schemas'] = json_schemas


@web.middleware
async def error_middleware(request: Any, handler: Any):
    try:
        response = await handler(request)
        if response.status != 404:
            return response

    except web.HTTPException as e:
        if e.status != 404:
            show_ex = e.text or e.body if e.text or e.body else str(e)
            log.info(f'web.HTTPException\n{show_ex}', exc_info=e)
            if DEBUG:
                return web.HTTPInternalServerError(text=f'{show_ex}\n {traceback.format_exc()}')
            else:
                return web.json_response({'error': show_ex}, status=e.status)
    except Exception as e:
        if DEBUG:
            return web.HTTPInternalServerError(text=f'Unknown error! {e}\n {traceback.format_exc()}')
        else:
            log.error(f'Unknown error! {e}\n', exc_info=e)
            return web.json_response({'error': 'Something has gone wrong!'}, status=500)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Nearest people service ')
    parser.add_argument('-d', '--debug', help='enables debug',
                        action='store_const', dest='debug', const=True,
                        default=False)
    parser.add_argument('-H', '--host', help='server host', default='0.0.0.0')
    parser.add_argument('-P', '--port', help="server port", default=80, type=int)
    args = parser.parse_args()

    DEBUG = args.debug
    TEST_SECRET = b't8df0Dxk4YvolRDnOiwee1Lez0xwOeAWCJigoKk__u4='

    # aiohttp.web.run_app actually doesn't support full asyncio.run functionality
    # also it's a way to run and control multiple apps in one thread if needed
    async def main():
        # run main from asyncio.run to set it's event loop for the app
        # for example in asyncio.create_task() or asyncio.Event()
        app = AppFactory(host=args.host,
                         port=args.port,
                         route_tables=[app_route_table],
                         debug=DEBUG,
                         secret=TEST_SECRET,
                         middlewares=[error_middleware],
                         events={'on_startup': [load_json_schemas]},
                         # qtree index could be potentially serialized to store in DB
                         app_args={'idxs': {}})

        await app.run_app()

    asyncio.run(main(), debug=DEBUG)
