import gzip
import os
import threading
import logging
import logging.config
import logging.handlers
import yaml


class GZipRotator:

    @staticmethod
    def setup_loggers(*args):
        for logger in args:
            for handler in logger.handlers:
                if isinstance(handler, logging.handlers.BaseRotatingHandler):
                    handler.rotator = GZipRotator()

    def __call__(self, source, dest):
        def exc_gzip():
            os.rename(source, dest)
            with open(dest, 'rb') as input_f:
                with gzip.open('{}.gz'.format(dest), 'wb') as output_f:
                    output_f.writelines(input_f)
            os.remove(dest)
        t = threading.Thread(target=exc_gzip)
        t.run()


def setup_logging(path='logging.yaml', level=logging.DEBUG):
    if os.path.exists(path):
        logs_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logs')
        if not os.path.exists(logs_path):
            os.mkdir(logs_path)
            os.mkdir(os.path.join(logs_path, 'app'))
            os.mkdir(os.path.join(logs_path, 'aiohttp'))
            os.mkdir(os.path.join(logs_path, 'asyncio'))
            os.mkdir(os.path.join(logs_path, 'access_logs'))

        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=level)


LOGGERS = ('app', 'aiohttp', 'asyncio', 'aiohttp.access')

setup_logging()
log = logging.getLogger('app')
GZipRotator.setup_loggers(*(logging.getLogger(name) for name in LOGGERS))
